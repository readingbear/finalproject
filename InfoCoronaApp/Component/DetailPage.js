import React from 'react';
import { StyleSheet,Text,View,Image,TouchableOpacity, TextInput } from 'react-native';
import {StatusBar} from 'expo-status-bar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {ImagesData} from './image.json'

export default class Detail extends React.Component {
    render(){
        var image='';
        for(let i = 1;i<=34;i++){
            if(i==this.props.route.params.datasend.attributes.FID){
                image = ImagesData[i-1];
            }
        }

        return(
            <View style ={styles.container}>
                <StatusBar translucent ={false} backgroundColor={"white"}/>
                    <Image source={{ uri: image }} style={{ width:150,height:150 }} />
                    <Text style={styles.title}>{this.props.route.params.datasend.attributes.Provinsi}</Text>
                    <View>
                        <Text style={styles.item}>Positif        : {this.props.route.params.datasend.attributes.Kasus_Posi} Orang</Text>
                        <Text style={styles.item}>Sembuh     : {this.props.route.params.datasend.attributes.Kasus_Semb} Orang</Text>
                        <Text style={styles.item}>Meninggal : {this.props.route.params.datasend.attributes.Kasus_Meni} Orang</Text>
                    </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor:'white'
      },
      title:{
          fontSize:24,
          fontWeight:'bold'
      },
      itembox:{
          width:200,
          height:200,
          paddingHorizontal:8
      },
      item:{
          fontSize: 18
      }
})