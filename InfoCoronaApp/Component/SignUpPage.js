import React from 'react';
import { StyleSheet,Text,View,Image,TouchableOpacity, TextInput } from 'react-native';
import {StatusBar} from 'expo-status-bar';
import {store} from './LoginPage';

const types = {
    INCREMENT1: 'INCREMENT1',
    INCREMENT2: 'INCREMENT2',
    INCREMENT3: 'INCREMENT3'
  }

export default class SignUp extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            userName: '',
            email: '',
            password: '',
            checkpassword:''
        }
      }
      
    signupHandler(){
        if(this.state.userName==''|| this.state.email==''||this.state.password== ''||this.state.checkpassword==''){
            alert("Harap Isi Data" );
        }
        else if (this.state.password==this.state.checkpassword){
            store.dispatch({ type: types.INCREMENT1,payload: this.state.userName  })
            store.dispatch({ type: types.INCREMENT2,payload: this.state.email })
            store.dispatch({ type: types.INCREMENT3,payload: this.state.password })
            this.props.navigation.navigate("Login")
        }
        else if (this.state.password!=this.state.checkpassword){
            alert("Password Tidak Sama" );
        }
    }
      
    render(){
        return(
            <View  style={styles.container}>
                <StatusBar translucent ={false} backgroundColor={"white"}/>
                    <Image source ={require('./Images/logo.png')} style ={styles.imgStyle} />
                <View>
                    <Text style={styles.txtstyle}>Username</Text>
                    <TextInput style={styles.boxstyle}
                        onChangeText={userName => this.setState({userName})}/>
                    <Text style={styles.txtstyle}>Email</Text>
                    <TextInput style={styles.boxstyle} 
                        onChangeText={email => this.setState({ email })}/>
                    <Text style={styles.txtstyle}>Password</Text>
                    <TextInput style={styles.boxstyle} 
                        secureTextEntry={true}
                        onChangeText={password => this.setState({ password })}/>
                    <Text style={styles.txtstyle}>Repeat Password</Text>
                    <TextInput style={styles.boxstyle} 
                        secureTextEntry={true}
                        onChangeText={checkpassword => this.setState({ checkpassword })}/>
                </View>
                <View style={{alignItems:'center',paddingTop:32,paddingBottom:16}}>
                    <TouchableOpacity style={styles.buttonstyle} onPress={()=> this.signupHandler()}>
                        <Text style={styles.txtbutton}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-around' ,paddingTop:2}}>
                    <Text style={{fontSize:15,color:'#003366'}}>Have an account?</Text>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("Login")} >
                            <Text style={{fontSize:15,color:'#3EC6FF'}}> Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgStyle:{
        width:100,
        height:100
    },
    txtstyle:{
        fontSize: 14, 
        color: '#003366',
        paddingTop: 16,
        paddingBottom: 4
    },
    boxstyle:{
        width:294,
        height:48,
        backgroundColor:'white',
        borderWidth:2,
        borderColor:'#003366',
        borderRadius: 8,
        paddingHorizontal:8
    },
    buttonstyle:{
        width:294,
        height:40,
        backgroundColor:'#3EC6FF',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical:2
    },
    txtbutton:{
        fontSize:15,
        color:'white',
        textAlign:'center'
    },
})