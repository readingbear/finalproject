import React from 'react';
import { StyleSheet,Text,View,Image,TouchableOpacity, TextInput,Dimensions } from 'react-native';
import {StatusBar} from 'expo-status-bar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const DEVICE = Dimensions.get('window')

export default class About extends React.Component {
    render(){    
        return(
            <View style ={styles.container}>
                <StatusBar translucent ={false} backgroundColor={"white"}/>
                <View style = {styles.navBar}>
                        <TouchableOpacity onPress={()=> this.props.navigation.toggleDrawer()}>
                            <Icon name = "menu" size = {30} />
                        </TouchableOpacity>
                        <Text style={{fontSize:24}}>About Me</Text>
                        <Icon name = "arrow-left" size = {30} color="transparent"/>
                </View>
                <View style={{paddingTop: 16}}>
                    <View style={{paddingHorizontal:16,flexDirection:'row', height:100,width: DEVICE.width}}>
                        <Image source={require('./Images/photo.jpg')} style={styles.photoprofile}/>
                        <View style={{paddingHorizontal:20}}>
                            <Text style={styles.name}>Farhan Ramadhan</Text>
                            <Text style={styles.work}>React Native Developer</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.portofolio}>
                    <Text style={styles.info}>Portofolio</Text>
                    <View style={styles.portliobox}>
                        <TouchableOpacity style={styles.tabItem}>
                            <Icon name="gitlab" size={43} color= '#3EC6FF'/>
                            <Text style={styles.icons}>@readingbear</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.accsocial}>
                    <Text style={styles.info}>Contact Me</Text>
                    <View style={styles.portliobox1}>
                        <TouchableOpacity style={styles.tabItem2}>
                            <Icon name="facebook" size={39} color= '#3EC6FF'/>
                            <Text style={styles.icons1}>Farhan Rmd</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem1}>
                            <Icon name="instagram" size={39} color= '#3EC6FF'/>
                            <Text style={styles.icons1}>@farhan.rmd</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItem1}>
                            <Icon name="twitter" size={39} color= '#3EC6FF'/>
                            <Text style={styles.icons1}>@farhan_rmdhan</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    photoprofile:{
        width: 100, 
        height: 100,
        borderRadius:50,
        borderWidth:2,
        borderColor:'#3EC6FF'
    },
    name:{
        paddingTop:24,
        fontSize:24,
        color:'#003366'
    },
    work:{
        paddingTop:8,
        fontSize:16,
        color:'#3EC6FF'
    },
    portofolio:{
        paddingTop:21,
        paddingHorizontal:16
    },
    accsocial:{
        paddingTop:9,
        paddingHorizontal:16
    },
    portliobox:{
        width:351,
        height:106,
        borderTopWidth:1,
        borderTopColor:'#003366',
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    portliobox1:{
        width:351,
        height:215,
        borderTopWidth:1,
        borderTopColor:'#003366',
        alignItems: 'center',
        justifyContent: 'center'
    },
    info:{
        fontSize:18,
        color:'#003366'
    },
    icons:{
        paddingTop:10,
        fontSize : 16,
        color:'#003366'
    },
    icons1:{
        paddingHorizontal:19,
        fontSize : 16,
        color:'#003366'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItem1: {
        paddingTop:30,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    tabItem2: {
        paddingTop:19,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    navBar:{
        height: 55,
        width: DEVICE.width,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
      }
})