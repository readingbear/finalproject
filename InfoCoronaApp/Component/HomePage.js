import React from 'react';
import {StatusBar} from 'expo-status-bar';
import { 
    View, 
    Text, 
    StyleSheet, 
    FlatList, 
    Image, 
    TouchableOpacity, 
    Dimensions,
    ImageBackground, 
    TextInput} 
from 'react-native';
import {ImagesData} from './image.json'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const DEVICE = Dimensions.get('window')

export default class Home extends React.Component {
  constructor(props) {
    super(props)
  }
  
  state = {
    data : []
  };
  
  componentDidMount() {
      this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("https://api.kawalcorona.com/indonesia/provinsi");
    const json = await response.json();
    this.setState({data: json});
  };

  render(){
      function imagelist(number){
        var image='';
          for(let i = 1;i<=34;i++){
              if(i==number){
                image = ImagesData[i-1];
              }
          }
        return(image)
        }
      return(
        <View style={styles.container}>
          <StatusBar translucent ={false} backgroundColor={"white"}/>
            <View style = {styles.navBar}>
                    <TouchableOpacity onPress={()=> this.props.navigation.toggleDrawer()}>
                      <Icon name = "menu" size = {30} />
                    </TouchableOpacity>
                    <Image source={require('./Images/logo.png')} style={{ width: 50, height: 50 }} />
                    <Icon name = "arrow-left" size = {30} color="transparent"/>
            </View>
            <FlatList
                data={this.state.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem = {({item}) => {
                return(
                  <TouchableOpacity onPress={()=> this.props.navigation.navigate("Details",{datasend:item})}>
                    <View style={styles.itemContainer}>
                      <Image source={{ uri: imagelist(item.attributes.FID) }} style={{ width:100,height:100 }} />  
                      <Text style={{fontSize:14,fontWeight:'bold',textAlign:'center'}}>{item.attributes.Provinsi}</Text>
                    </View>
                  </TouchableOpacity>
                )
                }}
                numColumns={2}
                />
        </View>
      )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  itemContainer: {
    width: DEVICE.width * 0.44,
    height:200,
    backgroundColor:"white",
    borderRadius:40,
    margin:8,
    justifyContent: "center",
    alignItems:"center",
    elevation:3
  },
  navBar:{
    height: 55,
    width: DEVICE.width,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
})