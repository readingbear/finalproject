import React from "react";
import { NavigationContainer} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Login from './LoginPage';
import SignUp from './SignUpPage';
import Home from './HomePage';
import About from './AboutPage';
import RealHome from './RealHomePage';
import Detail from './DetailPage';

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator initialRouteName="Login">
    <AuthStack.Screen
      name="Login"
      component={Login}
      options={{headerShown : false }}
    />
    <AuthStack.Screen
      name="SignUp"
      component={SignUp}
      options={{ title: "Sign Up",headerTitleAlign : 'center' }}
    />
    <AuthStack.Screen
      name="Home"
      component={DrawerScreen}
      options={{headerShown : false }}
    />
  </AuthStack.Navigator>
);

const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home} options={{headerShown : false }} />
    <HomeStack.Screen name="Details" component={Detail} options={{headerTitleAlign : 'center' }}/>
  </HomeStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Indonesia" component={RealHome} />
    <Tabs.Screen name="Provinsi" component={HomeStackScreen} options={{headerStyle:{height:55}}} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home">
    <Drawer.Screen name="Home" component={TabsScreen} />
    <Drawer.Screen name="About" component={About}/>
    <Drawer.Screen name="Logout" component={AuthStackScreen} options={{beforeRemove: true}}/>
  </Drawer.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none" initialRouteName="Auth">
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false
        }}
      />
      <RootStack.Screen
        name="App"
        component={DrawerScreen}
        options={{
          animationEnabled: false
        }}
      />
  </RootStack.Navigator>
);

export default function Index() {  
    return(
        <NavigationContainer>
            <RootStackScreen />
      </NavigationContainer>
    ) 
}