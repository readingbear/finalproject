import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    FlatList, 
    Image, 
    TouchableOpacity, 
    Dimensions, 
    TextInput} 
from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const DEVICE = Dimensions.get('window')

export default class RealHome extends React.Component {
  constructor(props) {
    super(props)
  }
  
  state = {
    data : []
  };
  
  componentDidMount() {
      this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("https://api.kawalcorona.com/indonesia");
    const json = await response.json();
    this.setState({data: json});
  };

  render(){
      return(
        <View style={styles.container}>
            <View style = {styles.navBar}>
                    <TouchableOpacity onPress={()=> this.props.navigation.toggleDrawer()}>
                      <Icon name = "menu" size = {30} />
                    </TouchableOpacity>
                    <Image source={require('./Images/logo.png')} style={{ width: 50, height: 50 }} />
                    <Icon name = "arrow-left" size = {30} color="transparent"/>
            </View>
            <FlatList
                data={this.state.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem = {({item}) => {
                  return (
                      <View style={styles.flatcontainer}>
                        <Image source={require('./Images/indonesia.png')} style={styles.ind} />
                        <Text style={styles.title}>{item.name}</Text>
                        <View >
                          <Text style={styles.item}>Positif        : {item.positif} Orang</Text>
                          <Text style={styles.item}>Sembuh     : {item.sembuh} Orang</Text>
                          <Text style={styles.item}>Meninggal : {item.meninggal} Orang</Text>
                        </View>
                      </View>
                      )
                }}
                />
        </View>
      )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  itemContainer: {
    width: DEVICE.width * 0.44,
    height:200,
	  margin:8,
    backgroundColor: 'white',
    justifyContent: "center",
    alignItems: "center",
  },
  navBar:{
    height: 55,
    width: DEVICE.width,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  ind : {
    width:DEVICE.width - 20, 
    height: (DEVICE.width - 20)/1.5
  },
  title:{
    fontSize:24,
    fontWeight:'bold',
    textAlign:'center',
    paddingBottom:10
  },
  item:{
    fontSize: 18
  },
  flatcontainer:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    paddingTop:100
  }
})