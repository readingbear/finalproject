import React from 'react';
import { StyleSheet,Text,View,Image,TouchableOpacity, TextInput } from 'react-native';
import {StatusBar} from 'expo-status-bar';
import { createStore } from 'redux'

// Membuat action types
const types = {
  INCREMENT1: 'INCREMENT1',
  INCREMENT2: 'INCREMENT2',
  INCREMENT3: 'INCREMENT3'
}
// Mendefinisikan initial state dari store
const initialState = {  userName: ['Farhan'],
                        email: ['farhanrmdjh@gmail.com'],
                        password: ['12345678'] }
// Membuat reducer
export const reducer = (state=initialState, action) => {
    const { userName,email,password } = state
    switch(action.type){
        case types.INCREMENT1 : {
            return { ...state, 
                userName: [action.payload, ...userName] 
            }
        }
        case types.INCREMENT2 : {
            return { ...state, 
                email: [action.payload, ...email] 
            }
        }
        case types.INCREMENT3 : {
            return { ...state, 
                password: [action.payload, ...password] 
            }
        }
    }
  return state
}

// Membuat store, menambahkan fungsi reducer dan nilai initial state
export const store = createStore(reducer)

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
        }
        this.UserName = []
        this.Email = []
        this.Password = []
    }
    loginHandler() {
        this.UserName = store.getState().userName
        this.Email = store.getState().userName
        this.Password = store.getState().userName
        var check = false;
        for(let i=0;i<this.UserName.length;i++){
            if(this.state.userName==this.UserName[i]||this.state.userName==this.Email[i]&&this.state.password==this.Password){
                check = true
            }
        }
        if(check==true){
            this.props.navigation.navigate("Home") 
        }
        else{
            alert("Username atau Password salah")
        }
      }    
    render(){
        return(
            <View  style={styles.container}>
                <StatusBar translucent ={false} backgroundColor={"white"}/>
                    <Image source ={require('./Images/logo.png')} style ={styles.imgStyle} />
                <View>
                    <Text style={styles.txtstyle}>Username/Email</Text>
                    <TextInput style={styles.boxstyle} onChangeText={userName => this.setState({ userName })}/>
                    <Text style={styles.txtstyle}>Password</Text>
                    <TextInput style={styles.boxstyle} secureTextEntry={true} onChangeText={password => this.setState({ password })}/>
                </View>
                <View style={{alignItems:'center',paddingTop:32,paddingBottom:16}}>
                    <TouchableOpacity style={styles.buttonstyle} onPress={()=> this.loginHandler() }>
                        <Text style={styles.txtbutton}>Login</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-around' ,paddingTop:2}}>
                    <Text style={{fontSize:15,color:'#003366'}}>Don't have an account?</Text>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("SignUp")} >
                            <Text style={{fontSize:15,color:'#3EC6FF'}}> Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgStyle:{
        width:100,
        height:100
    },
    txtstyle:{
        fontSize: 14, 
        color: '#003366',
        paddingTop: 16,
        paddingBottom: 4
    },
    boxstyle:{
        width:294,
        height:48,
        backgroundColor:'white',
        borderWidth:2,
        borderColor:'#003366',
        borderRadius: 8,
        paddingHorizontal:8
    },
    buttonstyle:{
        width:294,
        height:40,
        backgroundColor:'#3EC6FF',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical:2
    },
    txtbutton:{
        fontSize:15,
        color:'white',
        textAlign:'center'
    },
})